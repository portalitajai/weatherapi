var WeatherAPI = function () {
  this.key      = "58c237478565b4be8ffc6e74062c2e6b";
  this.url      = "http://api.openweathermap.org/data/2.5/weather";
  this.lang     = "pt";
  this.unidade  = "metric";
  this.cidade;
  this.pais;
}

WeatherAPI.prototype.validate = function() {
  var result = { data: true, error: "" };
  var getType = {};

  if( this.key.length === 0 )     return { data: false, error: "API KEY não está definida"};
  if( this.url.length === 0 )     return { data: false, error: "API URL não está definida"};
  if( this.lang.length === 0 )    return { data: false, error: "Linguágem de retorno não está definida"};
  if( this.unidade.length === 0 ) return { data: false, error: "Unidade de Medição não está definida"};
  if( this.cidade.length === 0 )  return { data: false, error: "Cidade não está definida"};
  if( this.pais.length === 0 )    return { data: false, error: "País não está definido"};

  return result;
}

// method to request the weather information
WeatherAPI.prototype.call = function( callbackFunction )
{
  var  validacao = this.validate();

  if( validacao.data ) {
    $.ajax({
      method: "GET",
      url: this.url + "?units="
                    + this.unidade
                    + "&lang="
                    + this.lang
                    + "&q="
                    + this.cidade
                    + ","
                    + this.pais
                    + "&&appid="
                    + this.key
    }).done(function( result ) {
      callbackFunction(result);
    }).fail(function(error) {
      console.log("Error", error);
    });
  }
  else {
    console.log("Error", { result: validacao.data, errorMessage: validacao.error } );
  }
};
