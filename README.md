# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Clone da API ###

O repositório da API deve ser baixado com o comando abaixo.

```bash
git clone git@bitbucket.org:portalitajai/weatherapi.git
```

### Definições ###

Esta API usa o serviço openweathermap.org para consultar a previsão do tempo por Cidade e País.

A documentação da API do serviço pode ser consultada em: [Documentação do Serviço](http://openweathermap.org/current)

### Parâmetros da API ###

key == API KEY;

url == URL da API;

lang == Linguagem do retorno da API;

unidade == Unidade de Medição do retorno da API;

cidade == Cidade que irá ser consultada;

pais == País da Cidade a ser consultada;

A URL já está configurada, a key está configurada uma que cadastrei, lang está definida como PT por default e unidade KM/H.

### Utilização da API ###

```javascript
function consularPrevisao() {
  var tempo = new WeatherAPI();
  tempo.cidade = "Itajaí";
  tempo.pais = "br";
  tempo.call( function( data ) {
    console.log(data);
  });
}
```

### UPDATES ###

Nenhuma atualização até o momento.